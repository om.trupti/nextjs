import Link from 'next/link';
import Router from 'next/router';

const Navbar = (backButton) => (
    <div className="container">
        <nav className="navbar navbar-dark hacknext">
            <ul>
                {backButton && (
                    <span onClick={() => Router.back()} className="back-button mr-2 text-left">
                        &#x2b05;
          </span>
                )}
                <li className="navbar-brand"><Link href="/">Home</Link></li>
                <li className="navbar-brand"><Link href="/about">About</Link></li>
                <li className="navbar-brand"><Link href="/blog">Blog</Link></li>
                <li className="navbar-brand"><Link href="/hackernews">Hacker News</Link></li>
                <li className="navbar-brand"><Link href="/otto">otto</Link></li>

            </ul>
        </nav>

    </div>
);
export default Navbar;