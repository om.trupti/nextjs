import Layout from './layout';

export default () => (
    <Layout title="Error!!!">
        <p class="container text-center mt-5 mb-5">Couldn't get that page, sorry! :-(</p>
    </Layout>
)