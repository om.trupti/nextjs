import fetch from 'isomorphic-unfetch';
import Layout from './layout';
import Error from 'next/error';
import CommentList from './CommentList';


class Story extends React.Component {

    static async getInitialProps({ req, res, query }) {
        let story;
        try {
            const storyId = query.id;
            const response = await fetch(`https://node-hnapi.herokuapp.com/item/${storyId}`
            );
            story = await response.json();

        } catch (err) {
            console.log(err);
            story = null;
        }
        return { story };
    }

    render() {
        const { story } = this.props;

        if (!story) {
            return <Error statusCode={503} />
        }
        return (
            <Layout title={story.title} backButton={true}>
                <div className="container text-left mt-3 mb-3 mx-auto">
                    <h5 className="hacknext_text">
                        <a className="hacknext_text" href={story.url}>{story.title}</a>
                    </h5>
                    <div className="story-details">
                        <strong className="mr-2">{story.points} points</strong>
                        <strong className="mr-2">{story.comments_count} comments</strong>
                        <strong className="mr-2">{story.time_ago} </strong>
                    </div>

                    {/* comment list */}
                    {story.comments.length > 0 ? (
                        <CommentList comments={story.comments} />
                    ) : (
                            <div>No comments for this story</div>
                        )}

                </div>
                <style jsx>{`
          main {
            padding: 1em;
          }
          .story-title {
            font-size: 1.2rem;
            margin: 0;
            font-weight: 300;
            padding-bottom: 0.5em;
          }
          .story-title a {
            color: #333;
            text-decoration: none;
          }
          .story-title a:hover {
            text-decoration: underline;
          }
          .story-details {
            font-size: 0.8rem;
            padding-bottom: 1em;
            border-bottom: 1px solid rgba(0, 0, 0, 0.1);
            margin-bottom: 1em;
          }
          .story-details strong {
            margin-right: 1em;
          }
          .story-details a {
            color: #f60;
          }
        `}</style>
            </Layout>
        )
    }
}
export default Story;