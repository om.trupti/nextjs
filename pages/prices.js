import React, { Component } from 'react'

class Prices extends Component {
    state = {
        currency: 'USD'
    }
    render() {
        let list = '';
        if (this.state.currency === "USD") {
            list =
                <li className="list-group-item col-8">
                    {this.props.bpi.bpi.USD.description} : <span className="badge badge-primary mr-3"> {this.props.bpi.bpi.USD.code} </span>
                    <storng>{this.props.bpi.bpi.USD.rate}</storng>
                </li>
        }
        else if (this.state.currency === 'GBP') {
            list =
                <li className="list-group-item col-8">
                    Bitcoin rate for {this.props.bpi.bpi.GBP.description} : <span className="badge badge-primary mr-3">{this.props.bpi.bpi.GBP.code} </span>
                    <storng>{this.props.bpi.bpi.GBP.rate}</storng>
                </li>
        } else if (this.state.currency === 'EUR') {
            list =
                <li className="list-group-item col-8">
                    Bitcoin rate for {this.props.bpi.bpi.EUR.description}
                    : <span className="badge badge-primary mr-3">
                        {this.props.bpi.bpi.EUR.code}
                    </span>
                    <storng>{this.props.bpi.bpi.EUR.rate}</storng>
                </li>
        }
        console.log("props===========", this.props);
        return (
            <div className="col">
                <select
                    onChange={e => this.setState({ currency: e.target.value })}
                    className="form-control col-4">
                    <option value="USD">USD</option>
                    <option value="GBP">GBP</option>
                    <option value="EUR">EUR</option>
                </select >
                <ul className="list-group">
                    Bitcoin rate for
                    {list}
                </ul>
                <br />



            </div >
        )
    }
}
export default Prices;
