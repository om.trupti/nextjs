import Document, { Head, Main, NextScript } from 'next/document';

class MyDocument extends Document {
    render() {
        return (
            <html lang="en">
                <Head>
                    {/* <title>how r u</title> */}
                    <meta name="description" content=" next js" />
                    <meta charsets="utf-8" />
                    <meta name="robots" content="noindex, nofollow" />
                    <meta name="viewport" content="width=device-width" />

                    <link rel="manifest" href="/static/manifest.json" />

                    <meta name="mobile-web-app-capable" content="yes" />
                    <meta name="apple-mobile-web-app-capable" content="yes" />
                    <meta name="application-name" content="hacker-next" />
                    <meta name="apple-mobile-web-app-title" content="hacker-next" />
                    <meta name="theme-color" content="#f60" />
                    <meta name="msapplication-navbutton-color" content="#f60" />
                    <meta
                        name="apple-mobile-web-app-status-bar-style"
                        content="black-translucent"
                    />
                    <meta name="msapplication-starturl" content="/" />
                    <meta
                        name="viewport"
                        content="width=device-width, initial-scale=1, shrink-to-fit=no"
                    />

                    <link
                        rel="icon"
                        type="image/png"
                        sizes="512x512"
                        href="/static/icons/icon-512x512.png"
                    />
                    <link
                        rel="apple-touch-icon"
                        type="image/png"
                        sizes="512x512"
                        href="/static/icons/icon-512x512.png"
                    />
                    <link
                        rel="icon"
                        type="image/png"
                        sizes="192x192"
                        href="/static/icons/icon-192x192.png"
                    />
                    <link
                        rel="apple-touch-icon"
                        type="image/png"
                        sizes="192x192"
                        href="/static/icons/icon-192x192.png"
                    />
                    <link rel="stylesheet" href="https://bootswatch.com/4/cerulean/bootstrap.min.css" />
                    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/nprogress/0.2.0/nprogress.min.css" />
                    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" />
                    {/* <link href="style.css" rel="stylesheet" /> */}

                </Head>
                <body>
                    <Main />
                    <NextScript />

                </body>

                <style global jsx>{`
                 body   {
                    font-family:'Roboto', sans-serif;
                                        }
                                        #nprogress .bar {
                    background: #29d !important;
                    }
                 `} </style>
            </html>
        )
    }
}
export default MyDocument;