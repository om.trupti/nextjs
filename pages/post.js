import Layout from './layout';
import { withRouter } from 'next/router';


const Post = ({ router }) => (
    <Layout title={router.query.title}>
        <p className="container mt-3">
            <h3>{router.query.title}</h3>
            Lorem ipsum
            Labore excepteur nostrud incididunt laboris velit ex in. Veniam cupidatat consectetur sunt id magna do veniam. Tempor commodo laboris in nisi cupidatat sit et deserunt ut officia fugiat irure amet cupidatat. Est incididunt elit qui veniam reprehenderit dolor amet non aliquip nisi. Et deserunt sunt Lorem dolore nostrud et cupidatat culpa sint cupidatat in minim ipsum et. Adipisicing Lorem nisi dolor deserunt irure dolor reprehenderit laboris sit velit.Adipisicing sint ex do nisi. Fugiat commodo deserunt ea dolor. Ut qui quis sit voluptate. In nostrud pariatur laboris in.Ipsum consectetur non et eiusmod. Voluptate sit ullamco minim sint cillum voluptate nostrud. Eu consequat labore ex anim pariatur do. Magna id nostrud voluptate velit minim commodo pariatur.
        </p>
    </Layout>
)

export default withRouter(Post);