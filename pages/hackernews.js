import fetch from 'isomorphic-unfetch';
import Layout from './layout';
import Error from 'next/error';
import StoryList from './storylist';
import Link from 'next/link';



class hackernews extends React.Component {
    static async getInitialProps({ req, res, query }) {
        let stories;
        console.log("query --- ---", query);
        let page;

        try {
            page = Number(query.page) || 1;
            const response = await fetch(`https://node-hnapi.herokuapp.com/news?page=${page}`);
            stories = await response.json();
            console.log(stories)

        } catch (err) {
            console.log(err);
            stories = [];
        }
        return { page, stories };
    }


    render() {
        const { stories, page } = this.props;

        if (stories.length === 0) {
            return <Error statusCode={503} />
        }


        return (
            <Layout>
                <div className="container mt-3 mb-3">
                    <div className="row">
                        <h4 className="col my-3 hacknext_text">Hacker Next</h4>
                        <p className="col text-right mb-0">
                            <Link href={`hackernews?page=${page + 1}`}>
                                <a className="hacknext_text">Next page ({page + 1})</a>
                            </Link>
                        </p>
                    </div>
                    <div className="bg-gray">  <StoryList stories={stories} /></div>
                </div>
            </Layout>
        )
    }
}

export default hackernews;