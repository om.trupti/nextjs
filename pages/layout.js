
import Head from 'next/head';
import Navbar from './navbar';
import Footer from './footer';
import Router from 'next/router';
import NProgress from 'nprogress';
import Link from "next/link";

Router.onRouteChangeStart = url => {
    console.log('==================', url);
    NProgress.start();
}

Router.onRouteChangeComplete = () => NProgress.done();
Router.onRouteChaneError = () => NProgress.done();

const Layout = (props) => (
    <div>
        <Head>
            <title>Nextjs</title>
            <link rel="stylesheet" href="https://bootswatch.com/4/cerulean/bootstrap.min.css" />
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/nprogress/0.2.0/nprogress.min.css" />
        </Head>
        <Navbar />
        {props.children}
        <Footer />
        <style global jsx>{`
                 body   {
                    font-family:'Roboto', sans-serif;
                                        }
                                        #nprogress .bar {
                    background: red !important;
                    }
                    .bg-gray{
                        background-color:#e8e8e8;
                    }
                    .hacknext_text{
                        color:#ffa52a;
                    }
                    .hacknext{
                        background-color:#ffa52a;
                        color:#fff !important;
                    }
                    .hacknext a{
                        color:#fff !important;
                    }
                 `} </style>
    </div>
);
export default Layout;