import Fetch from 'isomorphic-unfetch';
import Layout from './layout';
import Prices from './prices';
import { useState, useEffect } from 'react';

class Index extends React.Component {
    static async getInitialProps({ req, res, query }) {
        let stories;
        let page;

        try {
            // page = Number(query.page) || 1;
            // const response = await fetch(
            //     `https://node-hnapi.herokuapp.com/news?page=${page}`
            // );
            // stories = await response.json();

            const res = await Fetch('https://api.coindesk.com/v1/bpi/currentprice.json');
            const data = await res.json();
            console.log("data =============", data);
            return {
                bpi: data
            }


        } catch (err) {
            console.log(err);
            bpi = [];
        }

        return { page, stories };


    }



    componentDidMount() {
        if ("serviceWorker" in navigator) {
            navigator.serviceWorker
                .register("/service-worker.js")
                .then(registration => {
                    console.log("service worker registration successful", registration);
                })
                .catch(err => {
                    console.log(err.message)
                    console.warn("service worker registration failed", err.message);
                });
        }
    }

    render(props) {

        return (
            <Layout
                title="Hacker Next"
                description="A Hacker News clone made with Next.js"
            >
                <div className="container">
                    <h4 className="my-3 hacknext_text">Welcome to BitzPrice</h4>
                    {this.props.bpi.time.updated}<br />
                    <Prices bpi={this.props.bpi} />
                </div>


                <style jsx>{`
                    footer {
                        padding: 1em;
                    }
                    footer a {
                        font-weight: bold;
                        color: black;
                        text-decoration: none;
                    }
                 `}</style>
            </Layout>
        );
    }
}

export default Index;
