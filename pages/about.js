import Fetch from 'isomorphic-unfetch';
import Layout from './layout';
import { useState, useEffect } from 'react';


const About = (props) => (


    < Layout >
        <div className="container">
            <h4 className="my-3 hacknext_text">About</h4>
            {props.hookdata.params}
            {
                props.hookdata.map(data => {
                    // < ul className="list-group" >
                    <p className="list-group-item">{data.hitsPerPage}</p>
                    // </ul>
                })
            }


            <ul className="list-group">
                {/* <li className="list-group-item">{props}</li> */}
                <li className="list-group-item">Dapibus ac facilisis in</li>
                <li className="list-group-item">Morbi leo risus</li>
                <li className="list-group-item">Porta ac consectetur ac</li>
                <li className="list-group-item">Vestibulum at eros</li>
            </ul>
        </div>

    </Layout >
);

About.getInitialProps = async function () {
    const res = await fetch('https://hn.algolia.com/api/v1/search?query=reacthook');
    const data = await res.json();
    const statusCode = res.status > 200 ? res.status : false;
    console.log('statusCode', statusCode);
    if (statusCode) {
        return <Error />
    }


    console.log("about ===== ", data);
    return {
        hookdata: data.hits, statusCode

    }


}



export default About;