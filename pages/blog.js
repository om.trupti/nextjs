import Layout from './layout';
import Link from 'next/link';

const PostLink = ({ title }) => (
    <li>
        <Link href={`/Post?title=${title}`}>
            <a>{title}</a>
        </Link>
    </li>
);

export default () => (
    <Layout title="my blog" >

        <p className="container text-center mt-4 mb-4">
            <PostLink title="react" />
            <PostLink title="angular" />
            <PostLink title="vue" />

        </p>
    </Layout>
)